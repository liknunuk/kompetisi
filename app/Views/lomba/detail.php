<?php $this->extend('layout/template'); ?>
<?php $this->section('konten'); ?>

<!-- .container>.row>.col-lg-4.mx-auto -->
<div class="container">
    <div class="row">
        <div class="col-lg-4 mx-auto">
            <h1 class="text-center">Data Peserta Lomba</h1>
            <div class="list-group">
                <li class="list-group-item bg-info py-2">Jenis Lomba</li>
                <li class="list-group-item py-3"><?= $peserta['jnLomba']; ?></li>
                <li class="list-group-item bg-info py-2">Nama Peserta</li>
                <li class="list-group-item py-3"><?= $peserta['namaPeserta']; ?></li>
                <li class="list-group-item bg-info py-2">Putra/Putri</li>
                <li class="list-group-item py-3"><?= $peserta['jnKelamin']; ?></li>
                <li class="list-group-item bg-dark">
                    <button class="btn btn-danger" data-toggle="modal" data-target="#modal">Mundur!</button>
                    <a href="/lomba/" class="btn btn-success">Kembali</a>
                </li>
            </div>
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" role="modal-dialog" id="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Masukkan Nomor Pendaftaran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/lomba/mundur/" method="post">
                    <input type="text" name="noPendaftaran" class="form-control" id="noPendaftaran">
                    <input type="hidden" name="noUrut" value="<?= $peserta['noUrut']; ?>">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Mundur</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->endSection(); ?>