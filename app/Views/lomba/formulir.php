<?php $this->extend('layout/template'); ?>
<?php $this->section('konten'); ?>

<?php
switch ($lomba) {
    case 'Lukis':
        $awalan = 'L';
        break;
    case 'Pantomim':
        $awalan = 'P';
        break;
    case 'Badminton':
        $awalan = 'B';
        break;
    case 'Programming':
        $awalan = 'C';
        break;
}
?>

<div class="container">
    <div class="row">
        <div class="col">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1 class="display-4">LOMBA EKSTRA KURIKULER</h1>
                    <p class="lead">Aneka lomba memeriahkan hari jadi SLB Negeri Banjarnegara</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 mx-auto">
            <h3 class="text-center">FORMULIR PENDAFTARAN</h3>
            <h4 class="text-center">Mata Lomba : <?= $lomba; ?> </h4>
            <form action="<?= '/lomba/daftar'; ?>" method="post">

                <div class="form-group row">
                    <label for="noPendaftaran" class="col-sm-4">Nomor Pendaftaran</label>
                    <div class="col-sm-8">
                        <input type="text" name="noPendaftaran" class="form-control" readonly id="noPendaftaran" value="<?= $awalan . '-' . sprintf("%03d", $noPendaftaran + 1); ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="namaPeserta" class="col-sm-4">Nama Peserta</label>
                    <div class="col-sm-8">
                        <input type="text" name="namaPeserta" class="form-control" id="namaPeserta" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="jnKelamin" class="col-sm-4">Putra / Putri</label>
                    <div class="col-sm-8">
                        <div class="form-check form-check-inline">
                            <input type="radio" name="jnKelamin" id="jkPutra" value="Putra" class="form-check-input">
                            <label for="jkPutra" class="form-check-label">Putra</label>
                        </div>

                        <div class="form-check form-check-inline">
                            <input type="radio" name="jnKelamin" id="jkPutri" value="Putri" class="form-check-input">
                            <label for="jkPutri" class="form-check-label">Putri</label>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="jnLomba" value="<?= $lomba; ?>">
                <div class="form-group d-flex justify-conten-end">
                    <input type="submit" value="Daftar" class="btn btn-primary">
                </div>

            </form>
        </div>
    </div>
</div>

<?php $this->endSection(); ?>