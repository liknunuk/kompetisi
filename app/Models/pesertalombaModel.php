<?php

namespace App\Models;

use CodeIgniter\Model;

class PesertalombaModel extends Model
{
    //  Konfigurasi Model
    protected $table      = 'pesertalomba';
    protected $primaryKey = 'noUrut';
    protected $useSoftDeletes = true;
    protected $allowedFields = ['jnLomba', 'noPendaftaran', 'namaPeserta', 'jnKelamin'];
    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // daftar peserta berdasar jenis lomba
    public function pelomba($lomba, $noUrut = false)
    {
        if ($noUrut == false) {
            return $this->where('jnLomba', $lomba)->orderBy('namaPeserta')->findAll(0, 20);
        } else {
            return $this->find($noUrut);
        }
    }

    // fungsi untuk menambah dan mengubah data peserta lomba
    public function tambahperserta($data)
    {
        return $this->save($data);
    }

    // fungsi menghapus peserta lomba
    public function mundur($data)
    {
        $this->where($data)->delete();
        return $this->db->affectedRows();
    }

    // fungsi mendapatkan nomor pendaftaran baru
    public function nomorBaru($lomba)
    {
        $query = "SELECT COUNT(noPendaftaran) noPendaftaran FROM $this->table WHERE jnLomba = ?";
        $result = $this->db->query($query, [$lomba])->getResultArray();
        return $result;
    }

    // fungsi mencari nomor pendaftaran berdasar nomor urut
    public function cekNomor($noUrut)
    {
        return $this->where('noUrut', $noUrut)->findColumn('noPendaftaran');
    }
}
